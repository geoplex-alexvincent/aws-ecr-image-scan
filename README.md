# Bitbucket Pipelines Pipe: AWS ECR scan image

Scans docker images in the AWS Elastic Container Registry.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: ournova/aws-ecr-image-scan:0.4.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    IMAGE_NAME: "<string>"
    # TAG: "<string>" # Optional
    # FAIL_ON: "<string>" # Optional
    # MAX_CVSS_SCORE: "<string>" # Optional
    # TIMEOUT: "<int>" # Optional
    # IGNORE_UNSUPPORTED: "<boolean>" # Optional
    # ALL_FINDINGS: "<boolean>" # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key |
| AWS_DEFAULT_REGION (**) | AWS region |
| IMAGE_NAME            | The name of the image to scan in the ECR. The name should be the same as your ECR repository name.|
| TAG                   | Tag of image to scan. Default: `latest`.|
| FAIL_ON               | List of white space separated list results to fail on options `INFORMATIONAL, LOW, MEDIUM, HIGH, CRITICAL, UNDEFINED` default is `MEDIUM HIGH CRITICAL` |
| MAX_CVSS_SCORE        | Fail all vulnerabilities above this score, e.g. setting to `4.1` will fail above this value but this value will pass. Setting this value will ignore `FAIL_ON` if set. Default: `None` | 
| TIMEOUT               | Time out in seconds to wait for the scan to complete. Default: `60`. Set to `0` to wait max timeout (Current Max is 30min) |
| IGNORE_UNSUPPORTED    | By default image os versions not supported will fail the scan, set to `true` to ignore unsupported images. When image is supported scanning will automatically resume, to confirm check logs. Default: `false`. |
| ALL_FINDINGS          | By default only up to first 100 findings are retrieved, if you want to get all findings set this to `true`. Default: `false`. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have a IAM user configured with programmatic access, with the necessary permissions to scan docker images in your ECR repository.  
You also need to set up a ECR container registry if you don't already have on. Here is a [AWS ECR Getting started](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_GetStarted.html) guide from AWS on how to set up a new registry.

IMPORTANT! This pipe expects the docker image to be built already. You can see the examples below for more details.

## Examples

### Basic example:

Scanning the image with default options:

```yaml
script:
  # use the pipe to scan an image to AWS ECR
  - pipe: ournova/aws-ecr-image-scan:0.4.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
```


Example scanning the image with default options. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  # use the pipe to push the image to AWS ECR
  - pipe: ournova/aws-ecr-image-scan:0.4.0
    variables:
      IMAGE_NAME: my-docker-image
```

### Advanced example 1:

* Failing on certain vulnerabilities : `CRITICAL` only and  
* Shortening the wait time to 10 seconds for scans to complete before failure and  
* Specifying tag:  

```yaml
script:
  # use the pipe to push to AWS ECR
  - pipe: ournova/aws-ecr-image-scan:0.4.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
      FAIL_ON: CRITICAL
      TAG: v1.0.3-beta
      TIMEOUT: 10
```
### Advanced example 2:

* Failing on certain CVSS scores above 3.1 and
* Ignoring unsupported images and
* Specifying a tag and
* Get all findings:  

```yaml
script:
  # use the pipe to push to AWS ECR
  - pipe: ournova/aws-ecr-image-scan:0.4.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
      MAX_CVSS_SCORE: 3.1
      TAG: v1.0.5
      ALL_FINDINGS: true

```

## Acknowledgement

This project was initially based on the [aws-ecr-push-image](https://bitbucket.org/atlassian/aws-ecr-push-image) project by Atlassian.
Thank you for the template!
