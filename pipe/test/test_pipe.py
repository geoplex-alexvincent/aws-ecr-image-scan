import os  # noqa: F401
import sys
import pytest
import boto3  # noqa: F401
from botocore.exceptions import ClientError
from bitbucket_pipes_toolkit.test import PipeTestCase  # noqa: F401
from pipe import pipe
from pipe.test import test_utils


@pytest.fixture
def ecr_scanner():
    os.environ['IMAGE_NAME'] = 'Test_Image'
    ecr = pipe.ECRScan(pipe_metadata_file='pipe.yml', schema=pipe.schema, logger=pipe.logger, check_for_newer_version=False)
    return ecr


def test_get_client_ok(ecr_scanner, mocker):
    mocker.patch('boto3.client')
    client = ecr_scanner.get_client()
    assert(client)


def test_get_client_exception(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.side_effect = ClientError(test_utils.scan_not_found_error, 'describe_image_scan_findings')
    with pytest.raises(SystemExit):
        ecr_scanner.get_client()


def test_pipe_findings_fail(ecr_scanner):
    with pytest.raises(SystemExit):
        ecr_scanner.process_findings(test_utils.bad_scan_results)


def test_pipe_findings_pass(ecr_scanner):
    result = ecr_scanner.process_findings(test_utils.good_scan_results)
    assert(result is None)


def test_pipe_findings_all_findings(ecr_scanner, mocker):
    mocker.patch('time.sleep')
    mock_client = mocker.patch('boto3.client')
    ecr_scanner.variables['ALL_FINDINGS'] = True
    mock_client.return_value.describe_image_scan_findings.side_effect = [test_utils.bad_scan_results, test_utils.good_scan_results]
    with pytest.raises(SystemExit):
        ecr_scanner.process_findings(test_utils.bad_scan_results)
    assert(mock_client.return_value.describe_image_scan_findings.call_count == 2)


def test_pipe_findings_max_cvss(ecr_scanner, mocker):
    mocker.patch('time.sleep')
    ecr_scanner.variables['MAX_CVSS_SCORE'] = 5.3
    with pytest.raises(SystemExit):
        ecr_scanner.process_findings(test_utils.bad_scan_results)


def test_pipe_findings_max_cvss_no_findings(ecr_scanner, mocker):
    mocker.patch('time.sleep')
    ecr_scanner.variables['MAX_CVSS_SCORE'] = 11.0
    response = ecr_scanner.process_findings(test_utils.bad_scan_results)
    assert(response is None)


def test_start_scan(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_in_progress
    result = ecr_scanner.start_scan()
    assert(result == test_utils.start_scan_in_progress)


def test_start_scan_exception(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.side_effect = ClientError(test_utils.scan_limit_exceeded_error, 'start_scan')
    result = ecr_scanner.start_scan()
    assert(result == {'error': 'LimitExceededException'})


def test_start_scan_failed(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_failed
    with pytest.raises(SystemExit):
        ecr_scanner.start_scan()


def test_get_findings_exception(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.side_effect = ClientError(test_utils.scan_not_found_error, 'describe_image_scan_findings')
    result = ecr_scanner.get_findings()
    assert(result == {'error': 'ScanNotFoundException'})


def test_get_findings_good(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.good_scan_results
    result = ecr_scanner.get_findings()
    assert(result == test_utils.good_scan_results)


def test_get_findings_failed(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.start_scan_failed
    with pytest.raises(SystemExit):
        ecr_scanner.get_findings()


def test_get_findings_failed_unsupported_ignored(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    ecr_scanner.variables['IGNORE_UNSUPPORTED'] = True
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.unsupported_image_error
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ecr_scanner.get_findings()
    assert pytest_wrapped_e.value.code == 0


def test_get_findings_failed_unsupported_not_ignored(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.unsupported_image_error
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ecr_scanner.get_findings()
    assert pytest_wrapped_e.value.code == 1


def test_get_findings_in_progress(ecr_scanner, mocker):
    mocker.patch('time.sleep')
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.side_effect = [test_utils.in_progress_scan_results, test_utils.good_scan_results]
    response = ecr_scanner.get_findings()
    assert(response == test_utils.good_scan_results)


def test_get_next_scan(ecr_scanner, mocker):
    mocker.patch('time.sleep')
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.bad_scan_results
    response = ecr_scanner._get_next_scan_result('anexttoken')
    assert(response.get('nextToken') == test_utils.bad_scan_results.get('nextToken'))


def test_filter_by_cvss(ecr_scanner, mocker):
    findings = test_utils.bad_scan_results.get('imageScanFindings').get('findings')
    response = ecr_scanner._filter_by_cvss(findings, 5.3)
    assert(len(response) == 3)


def test_filter_by_cvss_empty(ecr_scanner, mocker):
    response = ecr_scanner._filter_by_cvss([], 5.3)
    assert(len(response) == 0)


def test_wait_for_scan(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mocker.patch('time.sleep')
    mock_client.return_value.describe_image_scan_findings.side_effect = [test_utils.in_progress_scan_results, test_utils.good_scan_results]
    result = ecr_scanner._wait_for_scan()
    assert(result == test_utils.good_scan_results)


def test_wait_for_scan_fail_with_unsupported(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mocker.patch('time.sleep')
    mock_client.return_value.describe_image_scan_findings.side_effect = [test_utils.in_progress_scan_results, test_utils.unsupported_image_error]
    with pytest.raises(SystemExit):
        ecr_scanner._wait_for_scan()


def test_wait_for_scan_failed(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mocker.patch('time.sleep')
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.start_scan_failed
    with pytest.raises(SystemExit):
        ecr_scanner._wait_for_scan()


def test_wait_for_scan_timeout(ecr_scanner, mocker):
    ecr_scanner.variables['TIMEOUT'] = 3
    mocker.patch('time.sleep')
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.describe_image_scan_findings.side_effect = [test_utils.in_progress_scan_results for i in range(3)]
    with pytest.raises(SystemExit):
        ecr_scanner._wait_for_scan()


def test_run_pass(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_complete
    mock_client.return_value.describe_image_scan_findings.return_value = test_utils.good_scan_results
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ecr_scanner.run()
    assert pytest_wrapped_e.value.code == 0


def test_run_scan_not_found_error(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_complete
    mock_client.return_value.describe_image_scan_findings.side_effect = [
        ClientError(test_utils.scan_not_found_error, 'test_run_scan_not_found_error'),
        test_utils.good_scan_results]
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ecr_scanner.run()
    assert pytest_wrapped_e.value.code == 0


def test_run_start_scan_fail(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_failed
    mock_client.return_value.describe_image_scan_findings.side_effect = [
        ClientError(test_utils.scan_not_found_error, 'test_run_scan_not_found_error'),
        test_utils.good_scan_results]
    with pytest.raises(SystemExit):
        ecr_scanner.run()


def test_run_start_scan_fail_fail(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.side_effect = ClientError(test_utils.scan_limit_exceeded_error, 'get_an_error')
    mock_client.return_value.describe_image_scan_findings.side_effect = [
        ClientError(test_utils.scan_not_found_error, 'test_run_scan_not_found_error'),
        ClientError(test_utils.scan_not_found_error, 'test_run_another_error')]
    with pytest.raises(SystemExit):
        ecr_scanner.run()


def test_run_start_scan_fail_pass_fail(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_complete
    mock_client.return_value.describe_image_scan_findings.side_effect = [
        ClientError(test_utils.scan_not_found_error, 'test_run_scan_not_found_error'),
        ClientError(test_utils.scan_not_found_error, 'test_run_another_error')]
    with pytest.raises(SystemExit):
        ecr_scanner.run()


def test_run_start_scan_fail_exception(ecr_scanner, mocker):
    mock_client = mocker.patch('boto3.client')
    mock_client.return_value.start_image_scan.return_value = test_utils.start_scan_complete
    mock_client.return_value.describe_image_scan_findings.side_effect = [
        ClientError(test_utils.scan_limit_exceeded_error, 'not_a_scan_not_found_exception')]
    with pytest.raises(SystemExit):
        ecr_scanner.run()


if __name__ == "__main__":
    sys.exit(pytest.main())
